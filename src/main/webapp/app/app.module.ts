import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { GeoDataAppSharedModule } from 'app/shared/shared.module';
import { GeoDataAppCoreModule } from 'app/core/core.module';
import { GeoDataAppAppRoutingModule } from './app-routing.module';
import { GeoDataAppHomeModule } from './home/home.module';
import { GeoDataAppEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    GeoDataAppSharedModule,
    GeoDataAppCoreModule,
    GeoDataAppHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    GeoDataAppEntityModule,
    GeoDataAppAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class GeoDataAppAppModule {}
